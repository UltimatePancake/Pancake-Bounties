from discord.ext import commands


class Topic:
    """Topic changer cog requested by iFox#0001"""

    __author__ = 'UltimatePancake'
    __requestor__ = 'iFox#0001'
    __request__ = 'https://cogboard.red/t/change-channel-topic-suggestion/150'
    __version__ = '0.1'
    __soundtrack__ = {
        'artist': 'Dead or Alive',
        'album': 'You spin me round'
    }

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def topic(self, ctx,  *, text):
        """Changes topic of current channel"""

        if len(text) > 1024:
            await self.bot.say("Message cannot have more than 1024 characters.")
        else:
            await self.bot.edit_channel(ctx.message.channel, topic=text)


def setup(bot):
    bot.add_cog(Topic(bot))