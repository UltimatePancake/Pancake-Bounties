import yaml
from os.path import isfile
from random import choice
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
from redbot.core import Config, checks, bank
from redbot.core.utils.chat_formatting import error, info, box
from tabulate import tabulate


class Mine:
    """Strike the earth!"""

    __author__ = 'UltimatePancake'
    __requestor__ = 'Skyde Panthera#8006'
    __request__ = 'https://cogboard.red/t/rng-mini-game-paid-v3/38'
    __version__ = '0.1'
    __soundtrack__ = {
        'artist': 'Krannholm',
        'album': 'Granting Death'
    }

    def __init__(self):
        self.config = Config.get_conf(self, identifier=8060460054)
        default_global = {
            "response_file": ""
        }
        default_user = {
            "salt": 0,
        }
        self.config.register_global(**default_global)
        self.config.register_user(**default_user)

    @commands.command()
    @commands.cooldown(1, 60, BucketType.user)
    async def mine(self, ctx):
        """Mine hard"""
        response_file = await self.config.response_file()
        if response_file == '':
            await ctx.send(error('Missing response configuration, contact bot owner.'))
            return
        result = await self._fetch_response()
        result_text = result['text']
        result_value = int(result['value'])
        if result_value > 0:
            await bank.deposit_credits(ctx.message.author, result_value)
            await self._add_salt(ctx.message.author, result_value)
        await ctx.send(f'{result_text} ({result_value} Salt)')

    async def _fetch_response(self):
        """Gets random response from file"""
        response_file = await self.config.response_file()
        with open(response_file, 'r') as stream:
            responses = yaml.load(stream)['responses']

        return choice(responses)

    async def _add_salt(self, user, salt):
        """Adds salt"""
        current_salt = await self.config.user(user).salt()
        await self.config.user(user).salt.set(current_salt + salt)
        return

    @commands.command()
    async def mineboard(self, ctx, amount: int=10):
        """Top miners"""
        user_data = await self.config.all_users()
        user_list = []
        for k, v in user_data.items():
            user = ctx.bot.get_user(k)
            username = f'{user.name}#{user.discriminator}'
            user_list.append({'User': username, 'Salt': v['salt']})

        sorted_list = sorted(user_list, key=lambda k: k['Salt'], reverse=True)
        sorted_list[0:amount]

        mineboard = tabulate(sorted_list, headers="keys")
        await ctx.send(box(mineboard))

    @commands.group(autohelp=True)
    @checks.is_owner()
    async def mineset(self, ctx, path=None):
        """Mine cog settings"""
        pass

    @mineset.command()
    @checks.is_owner()
    async def responsefile(self, ctx, path=None):
        """Set the responses file path"""
        if path is None:
            response_file = await self.config.response_file()
            if response_file == '':
                response_file = 'NOT CONFIGURED'
            await ctx.send(info(f'Current response file is {response_file}'))
            return

        if isfile(path):
            await self.config.response_file.set(path)
            await ctx.send(info(f'Response file set to {path}'))
        else:
            await ctx.send(error(f'{path} doesn\'t exist'))
